Современные технологии программирования
=======================================

.. warning::

   Устаревшая версия. Оставлена для пересдач. Актуальная версия здесь:

   https://csc-cpp.readthedocs.io/ru/2023/

.. toctree::
   :maxdepth: 1
   :caption: Осень

   s1/search-engine.rst
   s1/1-cmake.rst
   s1/2-parser.rst
   s1/3-indexer.rst
   s1/4-searcher.rst
   s1/5-books.rst
   s1/6-ffi.rst
   s1/7-binary-index.rst
   exam.rst
   code-style.rst
   books.rst

.. toctree::
   :maxdepth: 1
   :caption: Весна

   s2/0-repo.rst
   s2/1-time.rst
   s2/2-soundex.rst
   s2/3-smartptr.rst
   s2/4-iterator.rst
   s2/freq-dict.rst
   s2/rgr.rst

`Журнал <https://docs.google.com/spreadsheets/d/e/2PACX-1vSHlslviUBc5iV2JQFHkhODLRRHozRP5E4-FsADO-spgCex3fj54TnJnF0vEddInT3acYreNGIDFujP/pubhtml>`_


Лекции
------

0. `О курсе <_static/lectures/s1/00-about.pdf>`_
1. `CMake <_static/lectures/s1/01-cmake.pdf>`_
2. `Знакомство с C++ <_static/lectures/s1/02-intro.pdf>`_
3. `Статический анализ <_static/lectures/s1/03-static-analysis.pdf>`_
4. `Поиск <_static/lectures/s1/04-search.pdf>`_
5. `Указатели и ссылки <_static/lectures/s1/05-ptrs.pdf>`_
6. `Конкретные классы <_static/lectures/s1/06-classes.pdf>`_
7. `Инициализация и копирование <_static/lectures/s1/07-init.pdf>`_
8. `Обработка ошибок <_static/lectures/s1/08-errors.pdf>`_
9. `Кодировки <_static/lectures/s1/09-encodings.pdf>`_
10. `Наследование <_static/lectures/s1/10-inheritance.pdf>`_
11. `Кроссязыковые библиотеки <_static/lectures/s1/11-ffi.pdf>`_
12. Бинарные форматы данных: `Примеры <https://github.com/csc-cpp/cpp-examples/tree/main/12-binary>`_
13. `Перегрузка операторов <_static/lectures/s2/01-operators.pdf>`_
14. `Операторы сравнения <_static/lectures/s2/02-comparison.pdf>`_
15. `Функциональные объекты <_static/lectures/s2/03-function-objects.pdf>`_
16. `Шаблоны <_static/lectures/s2/05-templates.pdf>`_
17. `Шаблоны: примеры <_static/lectures/s2/06-templates-examples.pdf>`_
18. `Время жизни <_static/lectures/s2/07-lifetime.pdf>`_
19. `FFI Tutorial <_static/lectures/s2/08-ffi-tutorial.pdf>`_
20. `Итераторы <_static/lectures/s2/09-iterators.pdf>`_


Ресурсы
-------

* `C++ reference <https://en.cppreference.com/w/>`_
* `C++ Core Guidelines <http://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines>`_
* https://github.com/timsong-cpp/cppwp — HTML черновики стандартов языка C++
* https://compiler-explorer.com/

.. mermaid::

   gantt
       title Календарный план
       dateFormat YYYY-MM-DD
       axisFormat  %d.%m
       
       ЛР1, CMake           : 2022-09-05, 3w
       ЛР2, Парсер          : 2022-09-19, 3w
       ЛР3, Индексатор      : 2022-10-03, 3w
       ЛР4, Поиск           : 2022-10-17, 3w
       ЛР5, books.csv       : 2022-10-31, 3w
       ЛР6, FFI             : 2022-11-14, 3w
       ЛР7, Бинарный индекс : 2022-11-28, 4w
