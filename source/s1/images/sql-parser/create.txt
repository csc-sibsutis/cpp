Diagram(
  "CREATE", "TABLE", NonTerminal("TableName"),
  "(", NonTerminal("ColumnDef"),
       ZeroOrMore(",", NonTerminal("ColumnDef")),
  ")",
  ";")