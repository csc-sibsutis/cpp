Конспект по CMake
=================

Дисклеймер.

Катастрофически не хватает времени написать полноценное руководство по CMake.
Существует множество материалов:

* https://cmake.org/documentation — официальная документация;
* `An Introduction to Modern CMake <https://cliutils.gitlab.io/modern-cmake/>`_ — неофициальное, но полезное руководство.
* `Oh No! More Modern CMake - Deniz Bahadir - Meeting C++ 2019 <https://www.youtube.com/watch?v=y9kSr5enrSk>`_ —
  шикарный доклад, раскрывающий отличия современного CMake от старых версий.
  Может быть непростым для восприятия из-за обилия информации.

А еще десятки туториалов, проверять качество каждого из которых нет никакой
возможности. По опыту предыдущих лет — все еще слишком легко напороться на
руководство с устаревшими практиками.

Текст ниже призван расставить акценты на важных для курса вещах. Не претендует
на полноту и корректность — его даже никто не вычитывал. В любой непонятной
ситуации обращайтесь к официальной документации или авторитетным гайдам.


Система сборки, которая ничего не собирает
------------------------------------------

Исторически у C++ не было единой стандартной системы сборки. Как следствие,
параллельно существовало несколько инструментов (make, autotools, MSBuild…).
CMake стал не очередным `конкурирующим стандартом <https://xkcd.ru/927/>`_,
а более высокоуровневым инструментом поверх других систем сборки. Список
доступных генераторов см. в выводе команды ``cmake -G``.


Язык программирования, на котором не хочется писать
---------------------------------------------------

CMake — это простой скриптовый язык, см. `cmake-language(7)`_. Мы не будем
подробно останавливаться на его изучении, при необходимости обращайтесь к
документации. Отметим только две рекомендации.

1. По возможности старайтесь избегать дублирования. Для этого в языке есть
   функции/макросы.

2. Синтаксически язык может быть неудобен (в списках аргументов ключевые слова
   неотличимы от самих аргументов, а разбор таких аргументов выглядит как
   сделанный чужими для хищников, см. `cmake_parse_arguments`_), поэтому
   более-менее сложную логику имеет смысл выносить во внешние скрипты
   (например, на Python) и интегрировать посредством `add_custom_target`_
   или `add_custom_command`_.

Поскольку файлы ``CMakeLists.txt`` и ``*.cmake`` фактически являются скриптами,
а не конфигами фиксированной структуры, то обычно IDE плохо справляются с их
автоматической генерацией. Независимо от используемой вами IDE рекомендуется
писать весь код вручную.


Как пользоваться
----------------

В общем случае сборка проекта состоит из двух этапов:

1. Генерация файлов для системы сборки.
2. Непосредственно сборка.


Out-of-source build
~~~~~~~~~~~~~~~~~~~

Рекомендуемый способ сборки: все генерируемые файлы хранятся в отдельном
каталоге. Благодаря этому возможно параллельно собирать различные конфигурации:
Debug, Release, RelWithDebugInfo.

Для такой сборки нужно явно указать каталог, в котором будут храниться все
артефакты. Пример сборки двух конфигураций:

.. code-block:: bash

   $ git clone git@github.com:csc-bot/hello-cmake.git
   $ cd hello-cmake

   # Конфигурация
   $ cmake -S . -B build/Debug -DCMAKE_BUILD_TYPE=Debug
   $ cmake -S . -B build/Release -DCMAKE_BUILD_TYPE=Release

   # Сборка
   $ cmake --build build/Debug
   $ cmake --build build/Release

Если каталог с артефактами располагается в репозитории, то он должен быть в
``.gitignore``.


In-source build
~~~~~~~~~~~~~~~

Не рекомендуется использовать этот способ. Но есть соблазн запустить cmake
прямо в корне репозитория. Можете попробовать выполнить команду ``cmake .``, а
после посмотрите вывод ``git status``. Очевидно, что параллельная сборка
нескольких конфигураций в этом случае невозможна, да и я не могу придумать,
зачем вам может захотеться устраивать такой хаос в репозитории. Откатиться к
исходному состоянию можно командой ``git clean -df``.

Подробнее см. `Out-of-source build trees <https://gitlab.kitware.com/cmake/community/-/wikis/FAQ#out-of-source-build-trees>`_


Доменная модель
---------------

Сборка CMake состоит из *таргетов* (не будем переводить этот термин). Каждый
таргет может соответствовать физическому артефакту сборки или быть
псевдо-таргетом. Бинарные таргеты — исполняемые файлы и статические или
динамические библиотеки.

Также в доменной модели есть понятие *свойство*. Свойства могут быть
глобальными или принадлежать таргетам.

В основном работая с CMake вы создаете таргеты и заполняете их свойства.
В конечном счете CMake транслирует их в цели, зависимости и комнады мейкфайла
или конфиги другой выбранной системы сборки.

Для некоторых свойств CMake предоставляет достаточно высокоуровневые абстракции.
Например, для указания стандарта языка C++ мы можем определить свойство таргета
``CXX_STANDARD``:

.. code-block:: cmake

   set_target_properties(
     ${target_name}
     PROPERTIES
       CXX_STANDARD 17
   )

В зависимости от компилятора значение ``17`` будет оттранслировано в один из
ключей. Так, для gcc это будет ``-std=c++1z`` или ``-std=c++17`` (см.
`GNU-CXX.cmake <https://github.com/Kitware/CMake/blob/master/Modules/Compiler/GNU-CXX.cmake>`_).
Понятно, что физически невозможно поддерживать такие абстрактные значения для
всех компиляторов, поэтому часто разработчик должен самостоятельно следить,
поддерживается ли нужный ему флаг целевым компилятором. Например, опции для
настройки уровня предупреждений, необходимо отдельно настраивать для
компилятора MSVS.

Подробнее см. `cmake-buildsystem(7)`_.

Свойства могут обладать разными уровнем доступа. Эвристики для понимания их
смысла:

* PUBLIC — используется таргетом и всеми, кто зависит от него. Пример:
  пути к публичным заголовочным файлам библиотек.
* INTERFACE — не используется таргетом, но используется его клиентами. Пример:
  файлы header-only библиотек.
* PRIVATE — используется только таргетом. Пример: исходники.

В упомянутом выше примере ``hello-cmake`` мы получаем примерно такие таргеты и
свойства (многие, понятно, опущены):

.. graphviz::

   digraph G {
     bgcolor = "#ffffff00";
     app [shape=record, penwidth=0, label=<
       <table border="0" cellborder="1" cellspacing="0" cellpadding="4">
           <tr>
               <td colspan="2">app</td>
           </tr>
           <tr>
               <td>PRIVATE_SOURCES</td>
               <td>src/app/main.cpp</td>
           </tr>
           <tr>
               <td>PRIVATE_LINK_LIBRARIES</td>
               <td port="solver">solver</td>
           </tr>
       </table>>];

     solver [shape=record, penwidth=0, label=<
       <table border="0" cellborder="1" cellspacing="0" cellpadding="4">
         <tr>
           <td colspan="2">solver</td>
         </tr>
         <tr>
           <td rowspan="2">PRIVATE_SOURCES</td>
           <td>src/libsolver/libsolver/sqrt.cpp</td>
         </tr>
         <tr>
           <td>src/libsolver/libsolver/sqrt.hpp</td>
         </tr>
         <tr>
           <td>PUBLIC_INCLUDE_DIRECTORIES</td>
           <td>src/libsolver</td>
         </tr>
         <tr>
           <td>PRIVATE_LINK_LIBRARIES</td>
           <td>m</td>
         </tr>
       </table>>];

     app:solver -> solver;
   }

Здесь следует обратить внимание на две вещи.

Первая. Путь ``src/libsolver`` — публичный ``INCLUDE_DIRECTORIES`` у таргета
``solver``, поскольку он используется и самой библиотекой, и приложением.

Выполним сборку в режиме ``--verbose`` и убедимся в этом (вывод несколько
сокращен)::

   $ cmake --build ./build/Debug --verbose

   [ 25%] Building CXX object libsolver/sqrt.cpp.o
   /usr/bin/c++ -Isrc/libsolver -o build/libsolver/sqrt.cpp.o -c src/libsolver/sqrt.cpp

   [ 75%] Building CXX object src/app/CMakeFiles/app.dir/app/main.cpp.o
   /usr/bin/c++ -Isrc/libsolver -o build/app/main.cpp.o -c src/app/app/main.cpp

Как мы видим, опция ``-Isrc/libsolver`` была автоматически использована при
компиляции как библиотеки, так и зависящего от нее исполняемого файла. Этот
пример показывает, что вам не нужно вручную прописывать параметры, выходящие за
зону ответственности конкретного таргета — CMake протаскивает их автоматически
по зависимостям.

Вторая. Библиотека ``solver`` зависит от ``m``
— библиотеки с математическими функциями, причем зависимость приватная:
``solver`` использует функцию ``std::sqrt`` только в деталях реализации и не
выставляет ее через публичный интерфейс. Однако библиотека ``solver``
статическая. Вспомним, как компилируются такие библиотеки:

.. graphviz::

   digraph G {
     node [shape=box];
     bgcolor = "#ffffff00";
     rankdir=LR;
     "*.cpp" -> "*.s" [label = "Compiler"];
     "*.s" -> "*.o" [label = "Assembler"];
     "*.o" -> "*.a" [label = "Archiver"];
   }

В сборке статической библиотеки не участвует линкер.
Следовательно, использование флага ``-lm`` нужно отложить до этапа линковки.
CMake так и делает::

   /usr/bin/c++ app/main.cpp.o -o app ../libsolver/libsolver.a -lm

На этом примере мы видим, что CMake учитывает не только указание видимости
свойств, но и специфику сборки программ на языке C++.

В качестве упражнения измените тип библиотеки ``solver`` на ``SHARED`` и
посмотрите, как изменятся команды компиляции.


Как собрать компонент
---------------------

Пусть наш проект состоит из множества компонентов. Для того, чтобы собрать один
такой компонент, нужно:

1. `add_executable`_/`add_library`_ — создать соответствующий target.
2. Определить опции компиляции для таргета. Установку общих опций для ваших
   компонентов имеет смысл вынести в функцию, см. пример в ЛР1.
3. Если таргет — C++-артефакт, то установить свойство тартега ``CXX_CLANG_TIDY``.
4. `target_sources`_ — определить исходники таргета.
5. `target_include_directories`_ — определить ``INCLUDE_PATH`` для таргета. В
   случае библиотеки это каталог с ее публичными заголовочными файлами.
6. `target_link_libraries`_ — подключить необходимые библиотеки.

В принципе, CMake-код всего проекта можно написать в одном корневом
``CMakeLists.txt``. Иногда это оправданно. Для лучшей модульности и более явных
границ в рамках курса рекомендуется писать отдельный ``CMakeLists.txt`` для каждого компонента.

Тогда в соответствии с Canonical Project Structure структура проекта может выглядеть так::

   .
   |-- CMakeLists.txt
   |-- cmake
   |   `-- CompileOptions.cmake
   |-- src
   |   |-- CMakeLists.txt
   |   |-- calc
   |   |   |-- CMakeLists.txt
   |   |   `-- calc
   |   |       `-- main.cpp
   |   |
   |   `-- libcalc
   |       |-- CMakeLists.txt
   |       `-- libcalc
   |           |-- sum.cpp
   |           |-- sum.hpp
   |           `-- sum.test.cpp
   `-- thirdparty
       |-- CMakeLists.txt
       |-- cxxopts
       |   `-- CMakeLists.txt
       |-- fmtlib
       |   `-- CMakeLists.txt
       `-- googletest
           `-- CMakeLists.txt


Что осталось за кадром
----------------------

1. `ctest(1)`_/`enable_testing`_/`add_test`_ — работа с тестами. Пригодится.
   Ничего сложного, просто помните, что тест — такой же таргет.
2. `cmake-packages(7)`_ — будем использовать сторонние пакеты, но не будем
   оформлять свои.


.. _add_executable: https://cmake.org/cmake/help/latest/command/add_executable.html
.. _add_library: https://cmake.org/cmake/help/latest/command/add_library.html
.. _target_sources: https://cmake.org/cmake/help/latest/command/target_sources.html
.. _target_include_directories: https://cmake.org/cmake/help/latest/command/target_include_directories.html
.. _target_link_libraries: https://cmake.org/cmake/help/latest/command/target_link_libraries.html
.. _enable_testing: https://cmake.org/cmake/help/latest/command/enable_testing.html
.. _add_test: https://cmake.org/cmake/help/latest/command/add_test.html
.. _ctest(1): https://cmake.org/cmake/help/latest/manual/ctest.1.html
.. _cmake-packages(7): https://cmake.org/cmake/help/latest/manual/cmake-packages.7.html
.. _cmake-language(7): https://cmake.org/cmake/help/latest/manual/cmake-language.7.html
.. _cmake_parse_arguments: https://cmake.org/cmake/help/latest/command/cmake_parse_arguments.html
.. _add_custom_target: https://cmake.org/cmake/help/latest/command/add_custom_target.html
.. _add_custom_command: https://cmake.org/cmake/help/latest/command/add_custom_command.html

.. _cmake-buildsystem(7): https://cmake.org/cmake/help/latest/manual/cmake-buildsystem.7.html
