Вопросы к зачету
================

Лекция 1. «Системы сборки проектов: CMake»

1. Основные цели и задачи CMake, пример использования
2. `Основные команды CMake <https://cmake.org/cmake/help/latest/manual/cmake-commands.7.html>`_

Лекция 2. «Стандарты С++. Типы данных, области видимости»

3. `Основные новшества стандарта С++11 <https://en.cppreference.com/w/cpp/11>`_
4. `Объявление <https://en.cppreference.com/w/cpp/language/declarations>`_ и `определение <https://en.cppreference.com/w/cpp/language/definition>`_ переменных, `extern <https://en.cppreference.com/w/cpp/language/storage_duration>`_
5. `Области видимости <https://en.cppreference.com/w/cpp/language/scope>`_
6. `Ссылки, lvalue и rvalue <https://en.cppreference.com/w/cpp/language/reference>`_
7. const: константные ссылки, указатели, указатели на const
8. `auto <https://en.cppreference.com/w/cpp/language/auto>`_

Лекция 3. «new & delete, class, templates»

9. new & delete, для переменных и массивов
10. class – основные понятия (инкапсуляция)
11. шаблоны функций

Лекция 4. «Пространства имен, string, vector, iterators, range for»

12. Пространства имен
13. `std::string <https://en.cppreference.com/w/cpp/string/basic_string>`_ – основные операции
14. `std::vector <https://en.cppreference.com/w/cpp/container/vector>`_ – основные операции
15. `std::string_view <https://en.cppreference.com/w/cpp/string/basic_string_view>`_  – внутренее устройство и применимость
16. Понятие итератора для контейнерных типов данных
17. `Range-based for loop <https://en.cppreference.com/w/cpp/language/range-for>`_

Лекция 5. «Обработка исключений, умные указатели, RAII»

18. `try – catch – throw <https://en.cppreference.com/w/cpp/language/try_catch>`_
19. Smart pointers: `std::shared_ptr <https://en.cppreference.com/w/cpp/memory/shared_ptr>`_
20. Smart pointers: `std::unique_ptr <https://en.cppreference.com/w/cpp/memory/unique_ptr>`_
21. RAII: Resource Acquisition Is Initialization

Лекция 6. «C++ STL»

22. Основные потоки ввода-вывода, состояние потока, сброс буфера
23. STL: последовательные контейнеры
24. `STL: обобщенные алгоритмы <https://en.cppreference.com/w/cpp/algorithm>`_
25. STL: обобщенные алгоритмы – настройка с помощью предикатов
26. `STL: лямбда-выражения <https://en.cppreference.com/w/cpp/language/lambda>`_
27. STL: ассоциативные контейнеры

Лекция 7. «Классы в С++»

28. Абстракция и инкапсуляция, друзья класса
29. Указатель this, область видимости класса
30. Создание и удаление объектов класса
31. Конструкторы по умолчанию, делегирующие конструкторы   

Лекция 8. «Классы в С++: управление копированием»

32. `Копирующий конструктор <https://en.cppreference.com/w/cpp/language/copy_constructor>`_
33. `Оператор присваивания <https://en.cppreference.com/w/cpp/language/copy_assignment>`_
34. Спецификатор =default
35. Спецификатор =delete
36. Перемещение объектов

Лекция 9. «Классы в С++: перегрузка операторов»

37. Операторы для создания SmartPointers (Lippman, 14.7 Member Access Operators)
38. `Перегрузка операторов в классе <https://en.cppreference.com/w/cpp/language/operators>`_
39. `Конвертеры – преобразования, определенные пользователем <https://en.cppreference.com/w/cpp/language/cast_operator>`_

Лекция 10. «Классы в С++: наследование и полиморфизм»

40.  Статическое и `динамическое <https://en.cppreference.com/w/cpp/language/virtual>`_ связывание
41.  protected – уровень доступа
42.  Создание объекта наследника
43.  Области видимости и доступ к данным предков
44.  `Предотвращение наследования <https://en.cppreference.com/w/cpp/language/final>`_
45.  `Виртуальные функции, override и final <https://en.cppreference.com/w/cpp/language/virtual>`_
46.  `Абстрактные базовые классы <https://en.cppreference.com/w/cpp/language/abstract_class>`_

Лекция 11. «Паттерны проектирования: порождающие»

47. `Паттерн «Factory Method» <https://refactoring.guru/ru/design-patterns/factory-method>`_
48. `Паттерн «Abstract Factory» <https://refactoring.guru/ru/design-patterns/abstract-factory>`_
49. `Паттерн «Builder» <https://refactoring.guru/ru/design-patterns/builder>`_

Лекция 12. «Паттерны проектирования: структурные»

50. `Паттерн «Proxy» <https://refactoring.guru/ru/design-patterns/proxy>`_
51. `Паттерн «Adapter» <https://refactoring.guru/ru/design-patterns/adapter>`_
52. `Паттерн «Decorator» <https://refactoring.guru/ru/design-patterns/decorator>`_
53. `Паттерн «Facade» <https://refactoring.guru/ru/design-patterns/facade>`_
54. `Паттерн «Bridge» <https://refactoring.guru/ru/design-patterns/bridge>`_

Лекция 13. «Паттерны проектирования: поведенческие»

55. `Паттерн «Observer» <https://refactoring.guru/ru/design-patterns/observer>`_
56. `Паттерн «Visitor» <https://refactoring.guru/ru/design-patterns/visitor>`_
57. `Паттерн «Command» <https://refactoring.guru/ru/design-patterns/command>`_
