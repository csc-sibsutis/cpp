6. FFI
======

Постановка задачи
-----------------

Реализуйте обертку на языке C поверх поисковых функций библиотеки ``libfts``.
Напишите клиент на языке программирования, отличном от C и C++. Использование
библиотеки должно выглядеть максимально нативно для выбранного вами языка.
Инструменты для вызова C-функций из других языков:

- Python: `ctypes <https://docs.python.org/3/library/ctypes.html>`_
- PHP: `ffi <https://www.php.net/manual/en/class.ffi.php>`_
- Go: `cgo <https://pkg.go.dev/cmd/cgo>`_
- JavaScript: `node-ffi <https://github.com/node-ffi/node-ffi>`_,
  `node-ffi-napi <https://github.com/node-ffi-napi/node-ffi-napi>`_

Пример: https://github.com/csc-cpp/cpp-examples/tree/main/11-ffi/07-py-to-cpp-opaque
