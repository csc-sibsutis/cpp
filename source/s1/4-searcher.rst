4. Поиск и ранжирование
=======================

Цель
----

Реализовать алгоритм поиска документов по обратному индексу и простейшую 
функцию оценки релевантности документа.


Руководство
-----------

На входе у поисковика:

1. Текст запроса.
2. Конфиг: параметры парсера и прочие настройки, если потребуется.
3. Объект для доступа к индексу (IndexAccessor).

IndexAccessor предоставляет следующие возможности:

1. Получение содержимого документа по его идентификатору.
2. Получение списка документов по терму.

Алгоритм:

1. Распарсить текст запроса.
2. Для каждого терма запроса извлечь список документов, в которые он входит.
   На этом этапе уместно вычислить следующие величины:
   
   - :math:`\mathit{tf}(t, d)` — term frequency, количество вхождений терма
     :math:`t` в документ :math:`d`.
   
   - :math:`\mathit{df}(t)` — document frequency, количество документов,
     в которых встречается терм :math:`t`.

3. Для каждого документа вычислить оценку по формуле:

   .. math::

      \mathit{score}(q, d) = \sum_{t \in q}{\mathit{tf}\text{-}\mathit{idf}(t, d)}

   Где:

   .. math::

      \mathit{tf}\text{-}\mathit{idf}(t, d) = \mathit{tf}(t, d) \cdot \mathit{idf}(t)

   .. math::

      \mathit{idf}(t) = ln \left( \frac{N}{\mathit{df}(t)} \right)

   :math:`N` — общее количество документов в индексе.

4. Отсортировать результаты по убыванию :math:`\mathit{score}`. В качестве
   стабилизирующего фактора сортировки можно использовать id документа.


Пример
------

Пусть в индексе хранятся следующие документы:

.. code-block:: none

   id   text
   100  Hello World
   101  Bye World
   102  Hello Earth

Для компактности примера пропустим этап выделения n-грам и будем считать,
что каждое слово является термом. Тогда обратный индекс выглядит так:

.. code-block:: none

   bye:   {101: [0]}
   earth: {102: [1]}
   hello: {100: [0], 102: [0]}
   world: {100: [1], 101: [1]}

Поиск:

.. code-block:: yaml

   query: bye earth

   parsed_query: ["bye", "earth"]

   # Для каждого терма получаем список документов и считаем tf и df.
   bye:
      doc_ids: [101]
      tf:
         101: 1
      df: 1

   earth:
      doc_ids: [102]
      tf:
         102: 1
      df: 1

   score("bye earth", 101)
      = tf-idf("bye", 101) + tf-idf("earth", 101)
      = 1 * ln(3 / 1)      + 0
      ≈ 1.098612

Нетрудно заметить, что для документа 102 оценка будет такой же. Тогда
результирующая выдача примет вид:

.. code-block:: none

    id      score   text
   101   1.098612   Bye World
   102   1.098612   Hello Earth

Рассмотрим другой запрос:

.. code-block:: yaml

   query: Hello World

   parsed_query: ["hello", "world"]

   hello:
      doc_ids: [100, 102]
      tf:
         100: 1
         102: 1
      df: 2

   world:
      doc_ids: [100, 101]
      tf:
         100: 1
         101: 1
      df: 2

   score("hello world", 100)
      = tf-idf("hello", 100) + tf-idf("world", 100)
      = 1 * ln(3 / 2)        + 1 * ln(3 / 2)
      ≈ 0.810930
   
   score("hello world", 101)
      = tf-idf("hello", 101) + tf-idf("world", 101)
      = 0                    + 1 * ln(3 / 2)
      ≈ 0.405465

   score("hello world", 102)
      = tf-idf("hello", 102) + tf-idf("world", 102)
      = 1 * ln(3 / 2)        + 0
      ≈ 0.405465

Выдача примет вид:

.. code-block::

    id      score   text
   100   0.810930   Hello World
   101   0.405465   Bye World
   102   0.405465   Hello Earth


Структура проекта
-----------------

.. uml::

    skinparam backgroundcolor #fcfcfc
    skinparam linetype ortho

    package indexer {
        class IndexBuilder {
            + add_document(document_id, text): void
            + index(): Index
        }

        class Index {
            + docs
            + entries
        }

        IndexBuilder -[hidden]- Index

        class TextIndexWriter {
            + void write(path, index)
        }
   }

.. uml::

    skinparam backgroundcolor #fcfcfc
    skinparam linetype ortho

    package searcher {
        class TextIndexAccessor {
            + TextIndexAccessor(index_dir: fs::path)
            + config()
            + get_term_infos(string term): TermInfos
            + load_document(document_id): std::string
            + total_docs(): std::size_t
        }

        class Searcher <<free function>> {
            + search(query, index_accessor): std::vector<Result>
        }

        hide <<free function>> circle

        class Result {
            + score: double
            + document_id
        }

        Searcher --> Result
        TextIndexAccessor -[hidden]d-> Searcher
    }

Индексатор остается без изменений.

``TextIndexAccsessor`` — класс, предоставляющий доступ на чтение к данным
построенного индекса.


Дополнительно
-------------

Что произойдет, если индексация и поиск будут выполнены с разными настройками
парсера? Предложите и реализуйте решение этой проблемы.
