Литература
==========

Legality guide [#]_ (возможности языка):

1. `Stanley B. Lippman, C++ Primer, Fifth Edition, 2012 <https://www.goodreads.com/book/show/13358995-c-primer>`_

2. `Bjarne Stroustrup, The C++ Programming Language, Fourth Edition, 2013 <https://www.goodreads.com/book/show/12508475-the-c-programming-language>`_

3. `David Vandevoorde, C++ Templates: The Complete Guide, 2nd edition <https://www.goodreads.com/book/show/39404568-c-templates>`_

Morality guide (как пользоваться возможностями языка):

`Scott Meyers\' Effective C++ Book Series <https://www.goodreads.com/series/160060-effective-c>`_:

1. `Effective C++: 55 Specific Ways to Improve Your Programs and Designs <https://www.goodreads.com/book/show/105125.Effective_C_>`_
2. `More Effective C++ <https://www.goodreads.com/book/show/105123.More_Effective_C_>`_
3. `Effective STL: 50 Specific Ways to Improve Your Use of the Standard Template Library <https://www.goodreads.com/book/show/105124.Effective_STL>`_
4. `Effective Modern C++: 42 Specific Ways to Improve Your Use of C++11 and C++14 <https://www.goodreads.com/book/show/22800553-effective-modern-c>`_

Книги 1, 2 и 4 можно читать в произвольном порядке, см. интересующие вопросы по оглавлению.

.. [#] Терминология из `C++ FAQ - Learning C++ <https://isocpp.org/wiki/faq/how-to-learn-cpp>`_.
