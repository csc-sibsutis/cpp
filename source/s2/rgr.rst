РГР
===

Загрузка отчета
---------------

Курс в ЭИОС: https://eios.sibsutis.ru/course/view.php?id=2280

Кодовое слово: cpp-XX-YYY, где XX-YYY — группа. Например: cpp-IV-021


Цель
----

Реализовать шаблон класса для структуры данных в соответствии со своим
вариантом. Реализовать итераторы, совместимые с алгоритмами стандартной
библиотеки. Покрыть тестами.

Считаем, что вы полностью реализуете управление ресурсами, поэтому внутри
классов не используйте контейнеры стандартной библиотеки.

Начните с проектирования интерфейса: какие операции должен поддерживать
выбранный тип.

Репозиторий: https://classroom.github.com/a/3FQcgfFj

Вариантов немного, поэтому возможны повторы: не более двух человек на одном
варианте в пределах группы. При этом работа остается индивидуальной.


Шаблон отчета
-------------

.. role:: raw-html(raw)
   :format: html

`cpp_report_template.odt <../_static/s2/cpp_report_template.odt>`_

Шаблон подготовлен в LibreOffice и может некорректно отображаться в MS Office.

1. Для оформления используйте стили (Heading 2, Heading 3,  First Line Indent,
   preformatted text и др., см. шаблон).
2. :raw-html:`<span style="background-color: #81d41a;">Выделенные фрагменты</span>`
   — точки кастомизации. После вписывания своего текста уберите выделение.
3. В списке источников указывайте все использованные материалы, в т.ч. ссылки
   на блоги, youtube, онлайн курсы и ответы на stackoverflow. Для управления
   списком источников и ссылками можете использовать
   `Zotero <https://www.zotero.org/>`_.


Варианты
--------

1. FlatMap
2. FlatSet
3. HashMap 
4. HashSet
5. TreeMap 
6. TreeSet
7. SmallVector — вектор с дополнительным параметром N (количество элементов, хранимых на стеке).
8. Graph
9. Matrix
10. Vector + COW
11. String + COW
12. StableVector — вектор со стабильными итераторами/указателями на элементы.
13. PackedVector — вектор, использующий для хранения одного элемента N заданных бит.


Возможные проблемы
------------------

Варианты сформулированы без подробных описаний. Вам нужно самостоятельно
провести исследование: в чем особенности выбранной структуры данных и какой
должен быть публичный интерфейс. Обычно такой процесс строится итеративно,
с периодическими консультациями у преподавателя. Если вы впервые показываете
работу в последний момент или после срока, могут появиться неожиданные замечания
вплоть до полного несоответствия реализации ожиданиям. Это скажется на оценке,
и времени на исправления не останется.
