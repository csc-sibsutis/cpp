Дополнительно: частотный словарь
================================

Реализуйте программу для построения частотного словаря на трех языках:

- C++
- Java
- PHP

Измерьте время построения словаря. Сравните. Оптимизируйте.

Ход работы
----------

1. В качестве данных используйте файл
   `words-5m.txt <../_static/s2/words-5m.txt>`_.

   .. code::

      $ wget https://csc-cpp.readthedocs.io/ru/2022/_static/s2/words-5m.txt

2. Прочитайте слова из файла в последовательный контейнер.
3. Для каждого слово вычислите количество его появлений. Воспользуйтесь
   подходящей структурой данных.
4. Измерьте время подсчета количества слов.
5. Для самопроверки можете вывести общее количество уникальных слов:
   
   .. code:: bash
      
      $ sort words-5m.txt | uniq --count | wc --lines
      58410
   
   и топ-10 частотных слов:

   .. code:: bash

      $ sort words-5m.txt | uniq --count | sort --human --reverse | head --lines 10
       364031 the
       211763 of
       154576 and
       132248 to
       115603 in
       105114 a
        57269 is
        54994 that
        44676 for
        39406 it


