Ревью класса Polygon
====================

Репозиторий: https://classroom.github.com/a/Gk2jpRBt

Задание
-------

Выполните ревью приведенного кода по аналогии с ревью класса Complex из
предыдущего задания. **Не меняйте структуру программы.**

.. code-block:: cpp

   #include <vector>

   struct Point {
     double x = 0;
     double y = 0;
   };
   
   double some_work_placeholder(Point&) {
     return 0;
   }
   
   class Polygon {
    public:
     Polygon() : area{-1} {}
   
     void add_point(const Point pt) {
       area = -1;
       points.push_back(pt);
     }
   
     Point get_point(const int i) { return points[i]; }
   
     int get_num_points() { return points.size(); }
   
     double get_area() {
       if (area < 0) {
         // if not yet calculated and cached calculate now
         calc_area();
       }
       return area;
     }
   
    private:
     void calc_area() {
       area = 0;
       std::vector<Point>::iterator i;
       for (i = std::begin(points); i != std::end(points); ++i)
         area += some_work_placeholder(*i);
     }
   
     std::vector<Point> points;
     double area;
   };
   
   Polygon operator+(Polygon& lhs, Polygon& rhs) {
     auto ret = lhs;
     auto last = rhs.get_num_points();
     // concatenate
     for (auto i = 0; i < last; ++i) {
       ret.add_point(rhs.get_point(i));
     }
     return ret;
   }
   
   void f(const Polygon& poly) {
     const_cast<Polygon&>(poly).add_point({0, 0});
   }
   
   void g(Polygon& const poly) {
     poly.add_point({1, 1});
   }
   
   void h(Polygon* const poly) {
     poly->add_point({2, 2});
   }
   
   int main() {
     Polygon poly;
     const Polygon cpoly;
   
     f(poly);
     f(cpoly);
     g(poly);
     h(&poly);
   }
