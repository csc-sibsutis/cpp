Ревью класса Complex
=====================

Репозиторий: https://classroom.github.com/a/Gk2jpRBt


Задание
-------

Вам прислали на ревью следующий класс. Код содержит как явные ошибки, так и
примеры плохого стиля. Исправьте ошибки и объясните свои решения.

.. code-block:: cpp

   class Complex {
    public:
     Complex(double real, double imaginary = 0)
         : _real(real), _imaginary(imaginary) {}
   
     void operator+(Complex other) {
       _real = _real + other._real;
       _imaginary = _imaginary + other._imaginary;
     }
   
     void operator<<(std::ostream os) {
       os << "(" << _real << "," << _imaginary << ")";
     }
   
     Complex operator++() {
       ++_real;
       return *this;
     }
   
     Complex operator++(int) {
       Complex temp = *this;
       ++_real;
       return temp;
     }
   
    private:
     double _real, _imaginary;
   };


Ход работы
----------

1. Импортируйте исходник «как есть» отдельным коммитом.
2. Исправьте недочеты.
3. Оставьте комментарии: объясните исходную проблему и свое решение.
   Комментарии можно оставить прямо в коде или в pull request «Feedback».
4. Отправьте на проверку. Возможно, возникнет желание обсудить очно/голосом.
