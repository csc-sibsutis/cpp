3. Умные указатели
==================

.. code-block::

   cpp-labs
   |-- src/libcsc/libcsc/smartptr
   |   |-- SharedPtr.hpp
   |   `-- UniquePtr.hpp
   `-- test/libcsc/libcsc
       `-- smartptr.cpp


Реализуйте два шаблонных типа умных указателей:

- UniquePtr - тип без счетчика ссылок, безраздельно владеющий указателем.
- SharedPtr - тип со счетчиком ссылок для разделяемого владения указателем.

Реализуйте специальные методы в соответствии с
`Rule of five <https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines#c21-if-you-define-or-delete-any-copy-move-or-destructor-function-define-or-delete-them-all>`_.

Реализуйте следующие методы классов. В сигнатурах намеренно не указана
константность, определите самостоятельно, в каких случаях она нужна.

Для UniquePtr и SharedPtr:

.. cpp:function:: T& operator*()

.. cpp:function:: T* operator->()

.. cpp:function:: T* get()

.. cpp:function:: void reset(T* p = nullptr)

Только для SharedPtr:

.. cpp:function:: std::size_t use_count()

Покройте тестами, убедитесь в отсутствии утечек памяти.
