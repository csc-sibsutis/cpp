1. Перегрузка операторов
========================

Цель
----

1. Повторить основы перегрузки операторов.
2. Реализовать утилитарные классы для работы с временными интервалами.

.. code-block::

   cpp-labs
   |-- src/libcsc/libcsc/time
   |   |-- time.cpp (опционально)
   |   `-- time.hpp
   `-- test/libcsc/libcsc
       `-- time.cpp

Ход работы
----------

Создайте репозиторий для ЛР: :doc:`0-repo`

Реализуйте классы:

- Time — момент времени.
- TimeSpan — интервал.

Перегрузите операторы для реализации следующих операций:

- Time - Time = TimeSpan
- Time + TimeSpan = Time
- Time - TimeSpan = Time
- TimeSpan + TimeSpan = TimeSpan
- TimeSpan - TimeSpan = TimeSpan

Перегрузите оператор ``<<`` для вывода в формате: ``XXd XXh XXm XXs``
(пример: ``01d 23h 10m 11s```).

Реализуйте пользовательские литералы в пространестве имен
``<your_namespace>::literals``: ``_d``, ``_h``, ``_m``, ``_s``.

Перегрузите операторы сравнения. Если вы используете С++17, то операторы ``==``,
``!=``, ``<``, ``<=``, ``>``, ``>=``. Для С++20 достаточно определить оператор
``<=>``.


Ссылки
------

- `Affine Space Types <http://videocortex.io/2018/Affine-Space-Types/>`_
- `User-defined literals <https://en.cppreference.com/w/cpp/language/user_literal>`_
